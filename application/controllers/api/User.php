<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class User extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user');
    }

    public function login_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('password', $dataPost) && !empty($dataPost['password']))) {
            $login = $this->user->postLogin($dataPost);

            if (!empty($login)) {
                $this->response([
                    'responseCode'  => '00',
                    'responseMessage'  => 'Success',
                    'responseData'  => $login
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Incorrect Username or Password',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Incorrect Username or Password',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }

    public function logout_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username']))) {
            $logout = $this->user->logoutPost($dataPost);

            if ($logout > 0) {
                $this->response([
                    'responseCode'  => '00',
                    'responseMessage'  => 'Success Logout',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Failed Logout',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }

    public function cekToken_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('token', $dataPost) && !empty($dataPost['token']))) {
            $cekToken = $this->user->cekToken($dataPost);

            if (!empty($cekToken)) {
                $this->response([
                    'responseCode'  => '00',
                    'responseMessage'  => 'Success',
                    'responseData'  => $cekToken
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Invalid Token',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username or Token',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }
}
