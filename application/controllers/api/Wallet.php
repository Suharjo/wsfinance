<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Wallet extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Wallet_model', 'wallet');
        $this->load->model('User_model', 'user');
    }

    public function addWallet_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('token', $dataPost) && !empty($dataPost['token']))) {
            if (array_key_exists('wallet_type', $dataPost) && !empty($dataPost['wallet_type'])) {
                if (is_numeric($dataPost['wallet_type'])) {
                    if (array_key_exists('wallet_name', $dataPost) && !empty($dataPost['wallet_name'])) {
                        if (array_key_exists('amount', $dataPost) && !empty($dataPost['amount'])) {
                            if (is_numeric($dataPost['amount'])) {
                                // Cek Username
                                $cekUsername = $this->user->cekUsername($dataPost);

                                if (!empty($cekUsername)) {
                                    $dataPost['id'] =  $cekUsername[0]['id'];
                                    //Cek Token
                                    $cekToken = $this->user->cekToken($dataPost);
                                    if (!empty($cekToken)) {
                                        // Cek Wallet Type
                                        $cekWalletType = $this->wallet->cekWalletType($dataPost);

                                        if ($cekWalletType > 0) {
                                            //Cek Wallet Name
                                            $cekWalletName = $this->wallet->cekWalletName($dataPost);

                                            if ($cekWalletName == 0) {
                                                //Add Wallet
                                                $addWallet = $this->wallet->addWallet($dataPost);

                                                if ($addWallet) {
                                                    //Cek Wallet ID
                                                    $cekWalletId = $this->wallet->cekWalletId($dataPost);

                                                    if (!empty($cekWalletId)) {
                                                        $dataPost['wallet_id'] =  $cekWalletId[0]['wallet_id'];

                                                        //Add Transaction
                                                        $addTransaction = $this->wallet->insertFirstTransaction($dataPost);

                                                        if ($addTransaction) {
                                                            $this->response([
                                                                'responseCode'  => '00',
                                                                'responseMessage'  => 'Success',
                                                                'responseData'  => $dataPost
                                                            ], REST_Controller::HTTP_OK);
                                                        } else {
                                                            $this->response([
                                                                'responseCode'  => '97',
                                                                'responseMessage'  => 'Failed add Wallet',
                                                                'responseData'  => $dataPost
                                                            ], REST_Controller::HTTP_OK);
                                                        }
                                                    } else {
                                                        $this->response([
                                                            'responseCode'  => '98',
                                                            'responseMessage'  => 'Failed add Wallet',
                                                            'responseData'  => $dataPost
                                                        ], REST_Controller::HTTP_OK);
                                                    }
                                                } else {
                                                    $this->response([
                                                        'responseCode'  => '99',
                                                        'responseMessage'  => 'Failed add Wallet',
                                                        'responseData'  => $dataPost
                                                    ], REST_Controller::HTTP_OK);
                                                }
                                            } else {
                                                $this->response([
                                                    'responseCode'  => '99',
                                                    'responseMessage'  => 'Wallet Name Exist',
                                                    'responseData'  => $dataPost
                                                ], REST_Controller::HTTP_OK);
                                            }
                                        } else {
                                            $this->response([
                                                'responseCode'  => '99',
                                                'responseMessage'  => 'Invalid Wallet Type',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        }
                                    } else {
                                        $this->response([
                                            'responseCode'  => '99',
                                            'responseMessage'  => 'Invalid Token',
                                            'responseData'  => $dataPost
                                        ], REST_Controller::HTTP_OK);
                                    }
                                } else {
                                    $this->response([
                                        'responseCode'  => '99',
                                        'responseMessage'  => 'Invalid Username',
                                        'responseData'  => $dataPost
                                    ], REST_Controller::HTTP_OK);
                                }
                            } else {
                                $this->response([
                                    'responseCode'  => '99',
                                    'responseMessage'  => 'Invalid Wallet Amount',
                                    'responseData'  => $dataPost
                                ], REST_Controller::HTTP_OK);
                            }
                        } else {
                            $this->response([
                                'responseCode'  => '99',
                                'responseMessage'  => 'Empty Wallet Amount',
                                'responseData'  => $dataPost
                            ], REST_Controller::HTTP_OK);
                        }
                    } else {
                        $this->response([
                            'responseCode'  => '99',
                            'responseMessage'  => 'Empty Wallet Name',
                            'responseData'  => $dataPost
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'responseCode'  => '99',
                        'responseMessage'  => 'Invalid Wallet Type',
                        'responseData'  => $dataPost
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Empty Wallet Type',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username and Token',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }

    public function debit_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('token', $dataPost) && !empty($dataPost['token']))) {
            if (array_key_exists('category_id', $dataPost) && !empty($dataPost['category_id'])) {
                if (array_key_exists('wallet_id', $dataPost) && !empty($dataPost['wallet_id'])) {
                    if (array_key_exists('transaction_message', $dataPost)) {
                        if (array_key_exists('amount', $dataPost) && !empty($dataPost['amount'])) {
                            if (is_numeric($dataPost['amount'])) {
                                // Cek Username
                                $cekUsername = $this->user->cekUsername($dataPost);

                                if (!empty($cekUsername)) {
                                    $dataPost['id'] =  $cekUsername[0]['id'];
                                    //Cek Token
                                    $cekToken = $this->user->cekToken($dataPost);

                                    if (!empty($cekToken)) {
                                        $doDebit = $this->wallet->doDebit($dataPost);

                                        if ($doDebit) {
                                            $this->response([
                                                'responseCode'  => '00',
                                                'responseMessage'  => 'Success',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        } else {
                                            $this->response([
                                                'responseCode'  => '97',
                                                'responseMessage'  => 'Failed Transaction',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        }
                                    } else {
                                        $this->response([
                                            'responseCode'  => '99',
                                            'responseMessage'  => 'Invalid Token',
                                            'responseData'  => $dataPost
                                        ], REST_Controller::HTTP_OK);
                                    }
                                } else {
                                    $this->response([
                                        'responseCode'  => '99',
                                        'responseMessage'  => 'Invalid Username',
                                        'responseData'  => $dataPost
                                    ], REST_Controller::HTTP_OK);
                                }
                            } else {
                                $this->response([
                                    'responseCode'  => '98',
                                    'responseMessage'  => 'Invalid Amount',
                                    'responseData'  => $dataPost
                                ], REST_Controller::HTTP_OK);
                            }
                        } else {
                            $this->response([
                                'responseCode'  => '99',
                                'responseMessage'  => 'Empty Amount',
                                'responseData'  => $dataPost
                            ], REST_Controller::HTTP_OK);
                        }
                    } else {
                        $this->response([
                            'responseCode'  => '99',
                            'responseMessage'  => 'Empty Transaction Message',
                            'responseData'  => $dataPost
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'responseCode'  => '99',
                        'responseMessage'  => 'Empty Wallet ID',
                        'responseData'  => $dataPost
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Empty Category ID',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username and Token',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }

    public function kredit_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('token', $dataPost) && !empty($dataPost['token']))) {
            if (array_key_exists('category_id', $dataPost) && !empty($dataPost['category_id'])) {
                if (array_key_exists('wallet_id', $dataPost) && !empty($dataPost['wallet_id'])) {
                    if (array_key_exists('transaction_message', $dataPost)) {
                        if (array_key_exists('amount', $dataPost) && !empty($dataPost['amount']) && array_key_exists('amount_fee', $dataPost)) {
                            if (is_numeric($dataPost['amount'])) {
                                if (empty($dataPost['amount_fee'])) {
                                    $dataPost['amount_fee'] = 0;
                                }
                                // Cek Username
                                $cekUsername = $this->user->cekUsername($dataPost);

                                if (!empty($cekUsername)) {
                                    $dataPost['id'] =  $cekUsername[0]['id'];
                                    //Cek Token
                                    $cekToken = $this->user->cekToken($dataPost);

                                    if (!empty($cekToken)) {
                                        $doKredit = $this->wallet->doKredit($dataPost);

                                        if ($doKredit) {
                                            $this->response([
                                                'responseCode'  => '00',
                                                'responseMessage'  => 'Success',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        } else {
                                            $this->response([
                                                'responseCode'  => '97',
                                                'responseMessage'  => 'Balance is not enough',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        }
                                    } else {
                                        $this->response([
                                            'responseCode'  => '99',
                                            'responseMessage'  => 'Invalid Token',
                                            'responseData'  => $dataPost
                                        ], REST_Controller::HTTP_OK);
                                    }
                                } else {
                                    $this->response([
                                        'responseCode'  => '99',
                                        'responseMessage'  => 'Invalid Username',
                                        'responseData'  => $dataPost
                                    ], REST_Controller::HTTP_OK);
                                }
                            } else {
                                $this->response([
                                    'responseCode'  => '98',
                                    'responseMessage'  => 'Invalid Amount',
                                    'responseData'  => $dataPost
                                ], REST_Controller::HTTP_OK);
                            }
                        } else {
                            $this->response([
                                'responseCode'  => '99',
                                'responseMessage'  => 'Empty Amount',
                                'responseData'  => $dataPost
                            ], REST_Controller::HTTP_OK);
                        }
                    } else {
                        $this->response([
                            'responseCode'  => '99',
                            'responseMessage'  => 'Empty Transaction Message',
                            'responseData'  => $dataPost
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'responseCode'  => '99',
                        'responseMessage'  => 'Empty Wallet ID',
                        'responseData'  => $dataPost
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Empty Category ID',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username and Token',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }

    public function transfer_post()
    {
        $dataPost = $this->post();

        if ((array_key_exists('username', $dataPost) && !empty($dataPost['username'])) && (array_key_exists('token', $dataPost) && !empty($dataPost['token']))) {
            if (array_key_exists('category_id', $dataPost) && !empty($dataPost['category_id'])) {
                if (array_key_exists('wallet_id', $dataPost) && !empty($dataPost['wallet_id'])) {
                    if (array_key_exists('wallet_destination', $dataPost) && !empty($dataPost['wallet_destination'])) {
                        if (array_key_exists('transaction_message', $dataPost)) {
                            if (array_key_exists('amount', $dataPost) && !empty($dataPost['amount']) && array_key_exists('amount_fee', $dataPost)) {
                                if (is_numeric($dataPost['amount'])) {
                                    if (empty($dataPost['amount_fee'])) {
                                        $dataPost['amount_fee'] = 0;
                                    }
                                    // Cek Username
                                    $cekUsername = $this->user->cekUsername($dataPost);

                                    if (!empty($cekUsername)) {
                                        $dataPost['id'] =  $cekUsername[0]['id'];
                                        //Cek Token
                                        $cekToken = $this->user->cekToken($dataPost);

                                        if (!empty($cekToken)) {
                                            $doTransfer = $this->wallet->doTransfer($dataPost);

                                            if ($doTransfer) {
                                                $this->response([
                                                    'responseCode'  => '00',
                                                    'responseMessage'  => 'Success',
                                                    'responseData'  => $dataPost
                                                ], REST_Controller::HTTP_OK);
                                            } else {
                                                $this->response([
                                                    'responseCode'  => '97',
                                                    'responseMessage'  => 'Balance is not enough',
                                                    'responseData'  => $dataPost
                                                ], REST_Controller::HTTP_OK);
                                            }
                                        } else {
                                            $this->response([
                                                'responseCode'  => '99',
                                                'responseMessage'  => 'Invalid Token',
                                                'responseData'  => $dataPost
                                            ], REST_Controller::HTTP_OK);
                                        }
                                    } else {
                                        $this->response([
                                            'responseCode'  => '99',
                                            'responseMessage'  => 'Invalid Username',
                                            'responseData'  => $dataPost
                                        ], REST_Controller::HTTP_OK);
                                    }
                                } else {
                                    $this->response([
                                        'responseCode'  => '98',
                                        'responseMessage'  => 'Invalid Amount',
                                        'responseData'  => $dataPost
                                    ], REST_Controller::HTTP_OK);
                                }
                            } else {
                                $this->response([
                                    'responseCode'  => '99',
                                    'responseMessage'  => 'Empty Amount',
                                    'responseData'  => $dataPost
                                ], REST_Controller::HTTP_OK);
                            }
                        } else {
                            $this->response([
                                'responseCode'  => '99',
                                'responseMessage'  => 'Empty Transaction Message',
                                'responseData'  => $dataPost
                            ], REST_Controller::HTTP_OK);
                        }
                    } else {
                        $this->response([
                            'responseCode'  => '99',
                            'responseMessage'  => 'Empty Wallet Destination',
                            'responseData'  => $dataPost
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'responseCode'  => '99',
                        'responseMessage'  => 'Empty Wallet ID',
                        'responseData'  => $dataPost
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'responseCode'  => '99',
                    'responseMessage'  => 'Empty Category ID',
                    'responseData'  => $dataPost
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'responseCode'  => '99',
                'responseMessage'  => 'Empty Username and Token',
                'responseData'  => $dataPost
            ], REST_Controller::HTTP_OK);
        }
    }
}
