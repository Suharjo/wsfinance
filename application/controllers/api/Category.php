<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Category extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Category_model', 'category');
    }

    public function pemasukan_get()
    {
        $getPemasukan = $this->category->getPemasukan();

        $this->response([
            'responseCode'  => '00',
            'responseMessage'  => 'Success',
            'responseData'  => $getPemasukan
        ], REST_Controller::HTTP_OK);
    }

    public function pengeluaran_get()
    {
        $getPengeluaran = $this->category->getPengeluaran();

        $this->response([
            'responseCode'  => '00',
            'responseMessage'  => 'Success',
            'responseData'  => $getPengeluaran
        ], REST_Controller::HTTP_OK);
    }
}
