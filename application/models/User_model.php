<?php

class User_model extends CI_Model
{
    public function postLogin($dataPost)
    {
        $token = $this->generateToken(25);
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');
        $endTime = date('Y-m-d H:i:s', strtotime("+15 minutes", strtotime($date)));

        $this->db->set('token', $token);
        $this->db->set('expired_token', $endTime);
        $this->db->where('email', $dataPost['username']);
        $this->db->update('users');

        $this->db->limit(1);
        $login = $this->db->get_where('users', ['email' => $dataPost['username'], 'password' => $dataPost['password']])->result_array();

        return $login;
    }

    public function logoutPost($dataPost)
    {
        $this->db->set('token', null);
        $this->db->set('expired_token', null);
        $this->db->where('email', $dataPost['username']);
        $this->db->update('users');

        $result = $this->db->affected_rows();

        if ($result > 0) {
            $result = 1;
            return $result;
        } else {
            $result = 0;
            return $result;
        }
    }

    public function cekToken($dataPost)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        $this->db->select('*');
        $this->db->where('email', $dataPost['username']);
        $this->db->where('token', $dataPost['token']);
        $this->db->where('expired_token >', $date);
        $cekToken = $this->db->get('users')->result_array();

        return $cekToken;
    }

    public function cekUsername($dataPost)
    {
        $this->db->select('*');
        $this->db->where('email', $dataPost['username']);
        $this->db->limit('1');
        $cekUsername = $this->db->get('users')->result_array();

        return $cekUsername;
    }

    public function generateToken($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    //
}
