<?php

class Category_model extends CI_Model
{
    public function getPemasukan()
    {
        $this->db->select('*');
        $this->db->where('category_type', '1');
        $getPemasukan = $this->db->get('category')->result_array();

        return $getPemasukan;
    }

    public function getPengeluaran()
    {
        $this->db->select('*');
        $this->db->where('category_type', '2');
        $getPengeluaran = $this->db->get('category')->result_array();

        return $getPengeluaran;
    }
}
