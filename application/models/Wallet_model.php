<?php

class Wallet_model extends CI_Model
{
    public function cekWalletType($dataPost)
    {
        $this->db->select('*');
        $this->db->where('wallet_type_id', $dataPost['wallet_type']);
        $cekWalletType = $this->db->get('wallet_type')->num_rows();

        return $cekWalletType;
    }

    public function addWallet($dataPost)
    {
        $data = array(
            'wallet_owner' => $dataPost['id'],
            'wallet_name' => $dataPost['wallet_name'],
            'wallet_type_id' => $dataPost['wallet_type'],
            'amount' => $dataPost['amount']
        );

        $insert = $this->db->insert('wallet', $data);

        return $insert;
    }

    public function cekWalletId($dataPost)
    {
        $this->db->select('*');
        $this->db->where('wallet_owner', $dataPost['id']);
        $this->db->where('wallet_name', $dataPost['wallet_name']);
        $this->db->limit('1');
        $cekWalletId = $this->db->get('wallet')->result_array();

        return $cekWalletId;
    }

    public function cekWalletName($dataPost)
    {
        $this->db->select('*');
        $this->db->where('wallet_owner', $dataPost['id']);
        $this->db->where('wallet_name', $dataPost['wallet_name']);
        $cekWalletName = $this->db->get('wallet')->num_rows();

        return $cekWalletName;
    }

    public function insertFirstTransaction($dataPost)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        $data = array(
            'transaction_type' => 'D',
            'transaction_message' => 'DEPOSIT',
            'transaction_date' => $date,
            'saldo' => $dataPost['amount'],
            'amount' => $dataPost['amount'],
            'wallet_id' => $dataPost['wallet_id']
        );

        $insert = $this->db->insert('transaction', $data);

        return $insert;
    }

    public function doDebit($dataPost)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        //getSaldo
        $getSaldo = $this->db->get_where('wallet', ['wallet_id' => $dataPost['wallet_id'], 'wallet_owner' => $dataPost['id']])->result_array();
        $getSaldo =  $getSaldo[0]['amount'];

        $finalSaldo = $getSaldo + $dataPost['amount'];

        $data = array(
            'transaction_type' => 'D',
            'transaction_message' => $dataPost['transaction_message'],
            'transaction_date' => $date,
            'saldo' => $finalSaldo,
            'amount' => $dataPost['amount'],
            'wallet_id' => $dataPost['wallet_id'],
            'category_id' => $dataPost['category_id']
        );

        $insert = $this->db->insert('transaction', $data);

        $this->db->set('amount', $finalSaldo);
        $this->db->where('wallet_id', $dataPost['wallet_id']);
        $this->db->where('wallet_owner', $dataPost['id']);
        $this->db->update('wallet');

        return $insert;
    }

    public function doKredit($dataPost)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        //getSaldo
        $getSaldo = $this->db->get_where('wallet', ['wallet_id' => $dataPost['wallet_id'], 'wallet_owner' => $dataPost['id']])->result_array();
        $getSaldo =  $getSaldo[0]['amount'];

        $totalKredit = $dataPost['amount'] + $dataPost['amount_fee'];

        $finalSaldoFee = $getSaldo - $dataPost['amount_fee'];
        $finalSaldo = $getSaldo - $totalKredit;

        if ($finalSaldo >= 0) {
            $data = array(
                'transaction_type' => 'K',
                'transaction_message' => $dataPost['transaction_message'],
                'transaction_date' => $date,
                'saldo' => $finalSaldo,
                'amount' => $dataPost['amount'],
                'wallet_id' => $dataPost['wallet_id'],
                'category_id' => $dataPost['category_id']
            );

            $data_fee = array(
                'transaction_type' => 'K',
                'transaction_message' => 'FEE',
                'transaction_date' => $date,
                'saldo' => $finalSaldoFee,
                'amount' => $dataPost['amount_fee'],
                'wallet_id' => $dataPost['wallet_id'],
                'category_id' => '8888'
            );

            $this->db->insert('transaction', $data_fee);
            $this->db->insert('transaction', $data);

            $this->db->set('amount', $finalSaldo);
            $this->db->where('wallet_id', $dataPost['wallet_id']);
            $this->db->where('wallet_owner', $dataPost['id']);
            $this->db->update('wallet');

            $result = 1;
        } else {
            $result = 0;
        }

        return $result;
    }

    public function doTransfer($dataPost)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        //getSaldo
        $getSaldo = $this->db->get_where('wallet', ['wallet_id' => $dataPost['wallet_id'], 'wallet_owner' => $dataPost['id']])->result_array();
        $getSaldo =  $getSaldo[0]['amount'];

        $getSaldoDestination = $this->db->get_where('wallet', ['wallet_id' => $dataPost['wallet_destination'], 'wallet_owner' => $dataPost['id']])->result_array();
        $getSaldoDestination =  $getSaldoDestination[0]['amount'];

        $totalTransfer = $dataPost['amount'] + $dataPost['amount_fee'];

        $finalSaldoFee = $getSaldo - $dataPost['amount_fee'];
        $finalSaldo = $getSaldo - $totalTransfer;

        $finalSaldoDestination = $getSaldoDestination + $dataPost['amount'];

        if ($finalSaldo >= 0) {
            $data = array(
                'transaction_type' => 'K',
                'transaction_message' => 'TRANSER KE ID ' . $dataPost['wallet_destination'],
                'transaction_date' => $date,
                'saldo' => $finalSaldo,
                'amount' => $dataPost['amount'],
                'wallet_id' => $dataPost['wallet_id'],
                'category_id' => '9999'
            );

            $data_fee = array(
                'transaction_type' => 'K',
                'transaction_message' => 'FEE TRANSFER',
                'transaction_date' => $date,
                'saldo' => $finalSaldoFee,
                'amount' => $dataPost['amount_fee'],
                'wallet_id' => $dataPost['wallet_id'],
                'category_id' => '8888'
            );

            $data_transfer = array(
                'transaction_type' => 'D',
                'transaction_message' => 'TRANSFER DARI ID ' . $dataPost['wallet_id'],
                'transaction_date' => $date,
                'saldo' => $finalSaldoDestination,
                'amount' => $dataPost['amount'],
                'wallet_id' => $dataPost['wallet_destination'],
                'category_id' => '9999'
            );

            $this->db->insert('transaction', $data_fee);
            $this->db->insert('transaction', $data);
            $this->db->insert('transaction', $data_transfer);

            $this->db->set('amount', $finalSaldo);
            $this->db->where('wallet_id', $dataPost['wallet_id']);
            $this->db->where('wallet_owner', $dataPost['id']);
            $this->db->update('wallet');

            $this->db->set('amount', $finalSaldoDestination);
            $this->db->where('wallet_id', $dataPost['wallet_destination']);
            $this->db->where('wallet_owner', $dataPost['id']);
            $this->db->update('wallet');

            $result = 1;
        } else {
            $result = 0;
        }

        return $result;
    }
}
